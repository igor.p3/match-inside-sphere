using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class SpritePool : Pool<Image>
    {
        public Image Spawn(Vector3 position)
        {
            var image = Spawn(position, Quaternion.identity);
            image.gameObject.SetActive(true);
            return image;
        }
    }
}