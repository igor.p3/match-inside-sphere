using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Matchmania
{
    public class TextMeshPool : Pool<TextMeshProUGUI>
    {
        public TextMeshProUGUI SpawnTextMesh(Vector3 position)
        {
            var textMesh = Spawn(position, Quaternion.identity);
            textMesh.gameObject.SetActive(true);
            return textMesh;
        }
    }
}
