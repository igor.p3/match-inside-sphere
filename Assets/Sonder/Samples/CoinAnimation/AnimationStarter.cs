using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class AnimationStarter : MonoBehaviour
    {
        [SerializeField] private CoinsFlyAnimation coinsFlyAnimation;
        [SerializeField] [Range(1, 20)] private int minAmount = 2;
        [SerializeField] [Range(1, 20)] private int maxAmount = 10;

        public void StartEndLevelSequence()
        {
            var coins = Random.Range(minAmount, maxAmount + 1);

            var levelEndSequence = DOTween.Sequence()
                .AppendInterval(1.0f) // Do something 
                .AppendCallback(() => coinsFlyAnimation.SpawnCoins(coins, null))
                .AppendInterval(coinsFlyAnimation.Duration(coins))
                .AppendCallback(() => Debug.Log($"Coin animation completed!")) // or subscribe to OnAnimationAlmostCompleted
                .AppendInterval(1.0f) // Do next what you want 
                .Play();
        }


    }
}