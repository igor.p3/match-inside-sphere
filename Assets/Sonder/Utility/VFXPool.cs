using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Matchmania
{
    public class VFXPool : Pool<VFX>
    {
        public VFX Spawn(Vector3 position)
        {
            var vfx = Spawn(position, Quaternion.identity);
            vfx.gameObject.SetActive(true);
            return vfx;
        }
    }
}
