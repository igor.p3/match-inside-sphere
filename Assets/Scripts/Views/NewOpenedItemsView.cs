using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

namespace Matchmania
{
    public class NewOpenedItemsView : MonoBehaviour
    {
        public GameObject openedItemsScreen;
        [SerializeField] private GameObject[] itemsPositions;
        [SerializeField] private Button getItemsButton;
        [SerializeField] private GameObject victorScreenTab;

        public static List<Transform> openedItems = new List<Transform>();

        private bool isShowingItems;

        private void Start()
        {
            getItemsButton.onClick.AddListener(GetItemsButtonPressed);
        }
        private void GetItemsButtonPressed()
        {
            if (isShowingItems == false)
            {
                LevelView.CloseScreen(openedItemsScreen);
                victorScreenTab.SetActive(true);


                if (LevelController.CurrentLevelIndex == 10)
                {
                    RateUsView.Instance.ShowRateUsScreen();
                }

                ClearItems();
            }
        }
        public void SetItemsView()
        {
            ClearItems();
            StartCoroutine(ShowItems());
           
        }
        IEnumerator ShowItems()
        {
            isShowingItems = true;
            SceneLoadingController.UpdateUnlockedItems();

            for (int i = 0; i < GameController.lastUnlockedItems.Count; i++)
            {
                var item = Instantiate(GameController.lastUnlockedItems[i]);

                item.isRotate = true;
                item.rigidbody.isKinematic = true;
                item.transform.parent = itemsPositions[i].transform;
                item.transform.localPosition = Vector3.zero;
                item.transform.rotation = Quaternion.Euler(Vector3.zero);
                item.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);
                item.transform.DOScale(item.slotScale/2f, 0.25f);
                item.gameObject.layer = 10;
                if(item.transform.childCount > 0)
                {
              

                
                        foreach (var _item in item.transform.GetComponentsInChildren<Transform>())
                        {
                            _item.gameObject.layer = 10;
                        }
                    
                }
                item.isNewItemsScreen = true;
                openedItems.Add(item.transform);

                yield return new WaitForSeconds(0.1f);

                if(i == itemsPositions.Length -1)
                {
                    break;
                }
            }
            isShowingItems = false;
        }
        private void ClearItems()
        {
            foreach (var item in itemsPositions)
            { 
                if(item.transform.childCount != 0)
                {
                    Destroy(item.transform.GetChild(0).gameObject);
                }
            }
            openedItems.Clear();
        }
    }
}