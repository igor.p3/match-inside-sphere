using System;
using System.Collections;
using System.Collections.Generic;
using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TTEventView : MonoBehaviour
{
    [SerializeField] private GameObject eventDialogueObject;
    [Header("Event specifics")]
    [SerializeField] private Image[] eventIconImages;
    [SerializeField] private Image eventBannerImage;
    [SerializeField] private GameObject summerSunObj;
    
    [Header("Event graphics")]
    [SerializeField] private Sprite zooRamaBanner;
    [SerializeField] public Sprite zooRamaIcon;
    [SerializeField] private Sprite summerBreezeBanner;
    [SerializeField] public Sprite summerBreezeIcon;
    [SerializeField] private Sprite godsEscapeBanner;
    [SerializeField] public Sprite godsEscapeIcon;

    [Header("Amount to score")]
    [SerializeField] private TextMeshProUGUI textAmountToScore;
    [SerializeField] private TextMeshProUGUI textDialogueSlider;
    [SerializeField] private TextMeshProUGUI textDialogueTimer;
    [SerializeField] private Slider dialogueSlider;
    
    [Header("Reward")]
    [SerializeField] private Image[] rewardImage;
    [SerializeField] private TextMeshProUGUI[] textRewardCount;
    [SerializeField] public MainMenuView _mainMenuView;

    [Header("Buttons")] 
    [SerializeField] private Button playButton;
    [SerializeField] private Button menuHolderButton;
    [SerializeField] private Button openDialogueButton;

    [Header("Main menu progress")] 
    [SerializeField] private GameObject menuHolderObject;
    [SerializeField] private Slider menuSlider;
    [SerializeField] private TextMeshProUGUI textMenuTimer;
    [SerializeField] private TextMeshProUGUI textMenuSlider;
    
    [Header("Locations for TT bar")]
    [SerializeField] private RectTransform endScreenLocation;
    [SerializeField] private RectTransform menuScreenLocation;
    [SerializeField] private Canvas ttCanvas;
    [SerializeField] private GameObject levelEndBG;

    
    public void OpenDialogue() => eventDialogueObject.SetActive(true);

    private void SetEventSpecificGraphics()
    {
        var ttEventName = TTEventController.currentEvent;
        switch (ttEventName)
        {
            case TTEventDataModel.TTEventName.ZooRama:
                eventBannerImage.sprite = zooRamaBanner;
                AssignIconImages(zooRamaIcon);
                break;
            case TTEventDataModel.TTEventName.SummerBreeze:
                eventBannerImage.sprite = summerBreezeBanner;
                AssignIconImages(summerBreezeIcon);
                summerSunObj.SetActive(true);
                break;
            case TTEventDataModel.TTEventName.GodsEscape:
                eventBannerImage.sprite = godsEscapeBanner;
                AssignIconImages(godsEscapeIcon);
                break;
            default:
                return;
        }
    }

    private void AssignIconImages(Sprite eventIcon)
    {
        foreach (var eventIconImage in eventIconImages)
        {
            eventIconImage.sprite = eventIcon;
        }
    }

    public void ShowMenuHolder()
    {
        SetEventSpecificGraphics();
        menuHolderObject.SetActive(true);
    }

    public void ChangeLocationToEndScreen()
    {
        menuHolderObject.transform.position = endScreenLocation.transform.position;
        levelEndBG.SetActive(true);
        openDialogueButton.enabled = false;
        ttCanvas.sortingOrder = 2;

    }

    public void ChangeLocationToMenu()
    {
        menuHolderObject.transform.position = menuScreenLocation.transform.position;
        openDialogueButton.enabled = true;
        levelEndBG.SetActive(false);
        ttCanvas.sortingOrder = -1;
    }

    public void AssignLoot(TTEventDataModel.LootType lootType, int amount)
    {
        Sprite imageForLoot;
        switch (lootType)
        {
            case TTEventDataModel.LootType.Heart:
                imageForLoot = _mainMenuView.unlimHeartsSprite;             
                break;
            case TTEventDataModel.LootType.Hint:
                imageForLoot = _mainMenuView.hintSprite;
                break;
            case TTEventDataModel.LootType.Fan:
                imageForLoot = _mainMenuView.fanSprite;
                break;
            case TTEventDataModel.LootType.Coins:
                imageForLoot = _mainMenuView.coinsSprite;
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(lootType), lootType, null);
        }


        foreach (var textReward in textRewardCount)
        {
            textReward.text = lootType == TTEventDataModel.LootType.Heart
                ? TimeSpan.FromSeconds(amount).TotalMinutes.ToString("00")
                : amount.ToString();
        }
        

        foreach (var image in rewardImage)
        {
            image.sprite = imageForLoot;
        }
    }
    public void HideMenuHolder()
    {
        menuHolderObject.SetActive(false);
    }

    public void UpdateProgress(int amount, int maxAmount)
    {
        dialogueSlider.maxValue = maxAmount;
        dialogueSlider.value = amount;
        
        textDialogueSlider.text = $"{amount}/{maxAmount}";
        textAmountToScore.text = (maxAmount - amount).ToString();
        StartCoroutine(UpdateMenuAmountAnimation(amount, maxAmount));
    }

    private IEnumerator UpdateMenuAmountAnimation(int amount, int maxAmount)
    {
        menuSlider.maxValue = maxAmount;

        // If the amount to be updated is less than value of bar rn
        // Or if there is no difference in change
        // Just update the numbers
        var amountToGrow = amount - menuSlider.value;
        if (amount < menuSlider.value || amountToGrow == 0)
        {
            menuSlider.value = amount;
            textMenuSlider.text = $"{amount}/{maxAmount}";
            yield break;
        }
        
        // Otherwise update the numbers by growing the numbers
        for (int i = 0; i < amountToGrow; i++)
        {
            if (menuSlider.value >= amount) break;
            menuSlider.value += 1;
            textMenuSlider.text = $"{(int)menuSlider.value}/{maxAmount}";
            float amountToWait = 0.5f - (i * 0.1f);
            yield return new WaitForSeconds(Mathf.Clamp(amountToWait, 0.05f, 1f));
        }

        //TTEventController.Instance.CheckTokensAwards();
    }

    public void SpawnRewardsVFX(ParticleSpawner.ParticleObjects particleObjects, int amount)
    {
        ParticleSpawner.Instance.SpawnRewardVFX(particleObjects, amount, textMenuSlider.transform.position);
    }
    void Start()
    {
        playButton.onClick.AddListener(PlayButton);
        eventDialogueObject.SetActive(false);
        
    }

    private void PlayButton()
    {
        eventDialogueObject.SetActive(false);
        _mainMenuView.StartLevelButtonPressed();
    }
    
    public void UpdateTimersText(string textToShow)
    {
        textMenuTimer.text = textToShow;
        textDialogueTimer.text = textToShow;
    }
}
