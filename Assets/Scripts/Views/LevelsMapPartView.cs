using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Matchmania
{
    public class LevelsMapPartView : MonoBehaviour
    {
        [SerializeField] private LevelMapView levelMapView;

        [SerializeField] private Sprite yelloPath;
        [SerializeField] private Sprite levelKnobYellow;

        [SerializeField] private Image imageKnob;
        [SerializeField] private TextMeshProUGUI levelIndexCount;

        private Button claimRewardButton;

        public void SetPathView(int levelndex, bool isPassed)
        {
            levelIndexCount.text = (levelndex + 1).ToString();

            if (isPassed == true)
            {
                imageKnob.sprite = levelKnobYellow;
                GetComponent<Image>().sprite = yelloPath;
            }

            if (levelndex > 0 && (levelndex + 1) % 10 == 0 && levelndex >= LevelController.CurrentLevelIndex)
            {
                CreateLevelsChest();
            }
        }
        private void CreateLevelsChest()
        {
            Button claimRewardButton = Instantiate(levelMapView.levelsChestButton);
            claimRewardButton.gameObject.SetActive(true);
            claimRewardButton.transform.SetParent(imageKnob.transform);
            claimRewardButton.transform.localScale = Vector3.one;
            claimRewardButton.transform.localPosition = Vector3.zero;
            //claimRewardButton.onClick.AddListener(levelMapView.menuView.LevelsChestRewardButtonPressed);
            //claimRewardButton.onClick.AddListener(DestroyButton);
        }

        private void DestroyButton()
        {
            Destroy(claimRewardButton);
        }
    }
}