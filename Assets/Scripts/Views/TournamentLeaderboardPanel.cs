using System;
using System.Collections;
using System.Collections.Generic;
using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TournamentLeaderboardPanel : MonoBehaviour
{
    [Header("Basic info")]
    [SerializeField] private TextMeshProUGUI playerName; // This could be both stars or levels
    [SerializeField] private Image countryFlag;
    [SerializeField] private TextMeshProUGUI textTakenPlace;
    [SerializeField] private Image holderImage;

    [Header("Scores")]
    [SerializeField] private Image scoreImage; // This is cup or stars
    [SerializeField] private TextMeshProUGUI textScore; // This could be both stars or levels
    
    [Header("Rewards")]
    [SerializeField] private Image rewardImage; // This could be both coins or hints
    [SerializeField] private TextMeshProUGUI rewardAmount;

    [Header("Sprites")] 
    [SerializeField] private Sprite basicHolderTexture;
    [SerializeField] private Sprite goldenHolderTexture;
    
    
    public void SetView(string playerName, Sprite countryFlag, int takenPlace, int scoresGained, Sprite scoresImage, Sprite rewardImage, int lootAmount, bool isGolden)
    {
        this.playerName.text = playerName;
        this.countryFlag.sprite = countryFlag;
        textTakenPlace.text = takenPlace.ToString();
        scoreImage.sprite = scoresImage;
        textScore.text = scoresGained.ToString();
        this.rewardImage.sprite = rewardImage;
        rewardAmount.text = lootAmount.ToString();
        holderImage.sprite = isGolden ? goldenHolderTexture : basicHolderTexture;
    }

    public void HideRewardsSection()
    {
        rewardImage.gameObject.SetActive(false);
        rewardAmount.gameObject.SetActive(false);
    }
}
