using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Matchmania
{
    public class HardLevelDialoguesView : MonoBehaviour
    {
        [Header("Start level")]
        [SerializeField] private Image startLevelDialogue;
        [SerializeField] private Image startLevelHard;
        [Header("Star claim dialogue")]
        [SerializeField] private GameObject starClaimHard;
        [Header("OOM dialogues")]
        [SerializeField] private Image oobDialogue;
        [SerializeField] private GameObject oobDialogueHard;
        [SerializeField] private Image ootDialogue;
        [SerializeField] private GameObject ootDialogueHard;
        [Header("Level complete dialogues")]
        [SerializeField] private Image levelCompleteDialogue;
        [SerializeField] private GameObject levelCompleteHard;
        [Header("Fail dialogue")]
        [SerializeField] private GameObject failDialogueHard;
        [Header("Sprites for dialogues")]
        [SerializeField] private Sprite normalDialogueSprite;
        [SerializeField] private Sprite hardDialogueSprite;

        [SerializeField] private TextMeshProUGUI[] textLevelNumbers;

        public void SwitchToHard()
        {
            //startLevelDialogue.sprite = hardDialogueSprite;
            oobDialogue.sprite = hardDialogueSprite;
            ootDialogue.sprite = hardDialogueSprite;
            levelCompleteDialogue.sprite = hardDialogueSprite;

            starClaimHard.SetActive(true);
            oobDialogueHard.SetActive(true);
            ootDialogueHard.SetActive(true);
            levelCompleteHard.SetActive(true);
            failDialogueHard.SetActive(true);

        }

        public void SwitchToNormal()
        {
            //startLevelDialogue.sprite = normalDialogueSprite;
            oobDialogue.sprite = normalDialogueSprite;
            ootDialogue.sprite = normalDialogueSprite;
            levelCompleteDialogue.sprite = normalDialogueSprite;


            starClaimHard.SetActive(false);
            oobDialogueHard.SetActive(false);
            ootDialogueHard.SetActive(false);
            levelCompleteHard.SetActive(false);
            failDialogueHard.SetActive(false);
        }

        public void UpdateAllLevelNumbers(int levelNum)
        {
            foreach (var textLevelNumber in textLevelNumbers)
            {
                if (LevelController.Instance.isReviveScreenOpened == false)
                    textLevelNumber.text = $"Level {levelNum}";
                else textLevelNumber.text = $"Level {levelNum +1}";

            }
        }
    }
}