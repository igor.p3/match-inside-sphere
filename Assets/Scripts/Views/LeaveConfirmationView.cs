using System.Collections;
using System.Collections.Generic;
using Matchmania;
using TMPro;
using UnityEngine;

public class LeaveConfirmationView : MonoBehaviour
{
    #region Singleton
    private static LeaveConfirmationView _instance;
    public static LeaveConfirmationView Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<LeaveConfirmationView>();
            }

            return _instance;
        }
    }
    #endregion
    
    [SerializeField] private GameObject OOMConfirmationDialogue, OOTConfirmationDialogue;
    [SerializeField] private TextMeshProUGUI OOMConfirmationDialogueStars, OOTConfirmationDialogueStars;

    public bool ShowConfirmation()
    {
        if(LevelController.Instance.CurrentLevelStars == 0) return true;
        if (OOMConfirmationDialogue.activeSelf) return true;
        
        OOMConfirmationDialogue.SetActive(true);
        OOTConfirmationDialogue.SetActive(true);
        OOMConfirmationDialogueStars.text = LevelController.Instance.CurrentLevelStars.ToString();
        OOTConfirmationDialogueStars.text = LevelController.Instance.CurrentLevelStars.ToString();
        return false;
    }

    public void HideConfirmation()
    {
        OOMConfirmationDialogue.SetActive(false);
        OOTConfirmationDialogue.SetActive(false);
    }
    
}
