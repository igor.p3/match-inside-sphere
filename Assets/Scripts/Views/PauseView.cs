﻿using UnityEngine.UI;
using UnityEngine;
using DG.Tweening;

namespace Matchmania
{
    public class PauseView : MonoBehaviour
    {
        [SerializeField] private LevelView levelView;
        [SerializeField] private GameObject pauseScreen;

        [Header("Buttons")]

        [SerializeField] private Button restartButton;
        [SerializeField] private Button homeButton;
        [SerializeField] private Button closeButton;

        void Start()
        {
            PauseController.PauseOnGameEventHandler += OpenPauseScreen;
            PauseController.PauseOffGameEventHandler += ClosePauseScreen;

            homeButton.onClick.AddListener(HomeButtonPressed);
            restartButton.onClick.AddListener(RestartButtonPressed);
            closeButton.onClick.AddListener(CloseButtonPressed);
        }
        private void HomeButtonPressed()
        {
            PauseController.PauseGame(false);
            levelView.OnHomeButtonPressed();
        }
        private void RestartButtonPressed()
        {
            PauseController.PauseGame(false);
            levelView.RestartLevelButtonPressed();
        }
        private void CloseButtonPressed()
        {
            PauseController.PauseGame(false);
            LevelView.Instance.mainMenuHeader.SetActive(false);

            LevelController.Instance.IsPlayingLevel = true;
            LevelController.Instance.IsCanMakeTurn = true;
            StartCoroutine(LevelController.Instance.SetCanMakeTurn(0.05f));
        }
        private void ClosePauseScreen()
        {
            LevelView.CloseScreen(pauseScreen);
        }
        private void OpenPauseScreen()
        {
            LevelView.OpenScreen(pauseScreen);
            pauseScreen.transform.GetChild(0).GetComponent<Image>().DOFade(0.7f, 0);
            pauseScreen.transform.localScale = Vector3.one;

            LevelController.Instance.IsPlayingLevel = false;
            LevelController.Instance.IsCanMakeTurn = false;
        }
    }
}