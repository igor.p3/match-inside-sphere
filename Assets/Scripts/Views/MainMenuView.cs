﻿using System;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using DG.Tweening;
using System.Collections.Generic;
using System.Collections;

namespace Matchmania
{
    public class MainMenuView : MonoBehaviour
    {
        #region Singleton
        private static MainMenuView _instance;
        public static MainMenuView Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<MainMenuView>();
                }

                return _instance;
            }
        }
        #endregion

        public Action OnMenuStartLevelButtonPressed;

        [Header("References")]

        [SerializeField] private LevelView levelView;
        [SerializeField] private SFXConfig SFXConfig;
        [SerializeField] private TimeHeistView TimeHeistView;
        public CoinsFlyAnimation heartsFly;
        public CoinsFlyAnimation starsFly;
        public CoinsFlyAnimation coinsFly;
        public CoinsFlyAnimation chestCoinsFly;
        public CoinsFlyAnimation diomandsFly;

        [Header("Screens")]

        [SerializeField] private GameObject levelScreen;
        [SerializeField] private GameObject mainMenuScreen;
        [SerializeField] private GameObject levelsChestScreen;
        [SerializeField] private GameObject starsChestScreen;
        [SerializeField] private GameObject startLevelScreen;
        [SerializeField] private GameObject settingsScreen;
        public GameObject footerScreen;

        [Header("Texts")]

        [SerializeField] private TextMeshProUGUI coinsAmountText;
        [SerializeField] private TextMeshProUGUI coinsInShopAmountText;
        [SerializeField] private TextMeshProUGUI currentLevelIndexText;
        [SerializeField] private TextMeshProUGUI playerLevelIndexText;
        [SerializeField] private TextMeshProUGUI levelsChestCount;
        [SerializeField] private TextMeshProUGUI starsChestCount;

        [Header("Animators")]

        [SerializeField] private Animator levelsChestAnimator;
        [SerializeField] private Animator starsChestAnimator;
        [SerializeField] private Animator levelsChestRewardAnimator;
        [SerializeField] private Animator starsChestRewardAnimator;

        [Header("Sprites")]

        public Sprite hintSprite;
        public Sprite coinsSprite;
        public Sprite fanSprite;
        public Sprite unlimHeartsSprite;

        [Header("Images")]

        [SerializeField] private Image levelsChestRewardImage;
        [SerializeField] private Image starsChestRewardImage;
        [SerializeField] private Image levelChestImage;
        [SerializeField] private Image starChestImage;
        [SerializeField] private Image playerLevelFilledImage;

        [Header("Buttons")]

        [SerializeField] private Button claimLevelsChestRewardButton;
        [SerializeField] private Button claimStarsChestRewardButton;

        [SerializeField] private Button startClaimLevelsChestRewardButton;
        [SerializeField] private Button startClaimStarsChestRewardButton;

        [SerializeField] private Button levelsChestRewardButton;
        [SerializeField] private Button starsChestRewardButton;
        [SerializeField] private Button startLevelButton;
        [SerializeField] private Button settingsButton;
        [SerializeField] private Button closeSettingsButton;
        [SerializeField] private Button startGameButton;

        [Header("Sliders")]

        [SerializeField] Slider levelsChestSlider;
        [SerializeField] Slider starsChestSlider;

        [Header("Sprites")]

        [SerializeField] Sprite levelsChestSprite;
        [SerializeField] Sprite starsChestSprite;

        [Header("RectTransforms")]

        [SerializeField] private RectTransform starsChestTarget;
        [SerializeField] private RectTransform coinsFlyTarget;
        [SerializeField] private RectTransform diamondsFlyTarget;
        [SerializeField] private RectTransform heartFlyTarget;

        public static Tweener diamondsTweener;

        //void Update()
        //{
        //    if(Input.GetMouseButtonDown(0))
        //    FlyCoinsToLevelsChest(15);
        //}

        private void Start()
        {
            LevelView.GoHomeEventHandler += UpdateMainMenuView;
            UpdateMainMenuView();
            startLevelButton.onClick.AddListener(StartLevelButtonPressed);
            startGameButton.onClick.AddListener(StartTheGame);
            CurrencyController.OnChangeCurrencyEventHandler += UpdateMainMenuView;

            claimLevelsChestRewardButton.onClick.AddListener(ClaimLevelsChestRewardButtonPressed);
            claimStarsChestRewardButton.onClick.AddListener(ClaimStarsChestRewardButtonPressed);

            settingsButton.onClick.AddListener(OpenSettingsButtonPressed);
            closeSettingsButton.onClick.AddListener(CloseSettingsButtonPressed);

            UpdateMainMenuView();
            CurrencyController.OnChangeCurrencyEventHandler += UpdateMainMenuView;

            UpdatePlayerLevel();

            PlayerLevelUpgrade.OnUpdateEventHandler += UpdatePlayerLevel;

            startClaimStarsChestRewardButton.onClick.AddListener(StarsChestRewardButtonPressed);
            startClaimLevelsChestRewardButton.onClick.AddListener(LevelsChestRewardButtonPressed);

            levelsChestRewardButton.onClick.AddListener(OpenLevelsChestRewardScreen);
            starsChestRewardButton.onClick.AddListener(OpenStarsChestRewardScreen);

            if (LevelController.CurrentLevelIndex == 0)
            {
                StartLevelButtonPressed();
            }
      

        }
        public void OpenSettingsButtonPressed()
        {
            //if(LevelController.Instance.isReviveScreenOpened == false)
            LevelView.OpenScreen(settingsScreen);
        }
        public void CloseSettingsButtonPressed()
        {
            LevelView.CloseScreen(settingsScreen);
        }
        public void UpdateCoinsInWallet()
        {
            coinsAmountText.text = CurrencyController.Coins.ToString();
            coinsInShopAmountText.text = CurrencyController.Coins.ToString();
        }
        public void UpdatePlayerLevel()
        {
            playerLevelIndexText.text = PlayerLevelUpgrade.GetPlayerLevel().ToString();
            float value = (float)CurrencyController.StarsChestCount / 1000f;
            playerLevelFilledImage.fillAmount = value;
        }
        public void StartLevelButtonPressed()
        {
            if (HeartsController.Instance.HeartsCheck() == false) return;

            if (TimeHeistView.isGivingReward == false)
            {
                OnMenuStartLevelButtonPressed?.Invoke();
                // todo uncomment this line to get the start level screen back  
                //startLevelScreen.SetActive(true);
                // todo Comment this string to get the start level screen back
                StartTheGame();
                footerScreen.SetActive(false);
            }
        }
  

        private void StartTheGame()
        {
            LevelController.Instance.StartLevel(LevelController.CurrentLevelIndex);
            levelScreen.SetActive(true);
            startLevelScreen.SetActive(false);
            mainMenuScreen.SetActive(false);
            SFXConfig.PlaySoundEffect(SFXConfig.buttonClick);
            LevelController.Instance.FadeEffect();

        }
        public void UpdateMainMenuView()
        {
            currentLevelIndexText.text = (LevelController.CurrentLevelIndex + 1).ToString();
            coinsAmountText.text = CurrencyController.Coins.ToString();
            coinsInShopAmountText.text = CurrencyController.Coins.ToString();



            levelsChestCount.text = CurrencyController.LevelsChestCount.ToString() + "/" + levelsChestSlider.maxValue.ToString();
            levelsChestSlider.value = CurrencyController.LevelsChestCount;

            starsChestCount.text = CurrencyController.StarsChestCount.ToString() + "/" + starsChestSlider.maxValue.ToString();
            starsChestSlider.value = CurrencyController.StarsChestCount;

            if (CurrencyController.StarsChestCount >= starsChestSlider.maxValue)
            {
                starsChestCount.text = "Open";
                starsChestRewardButton.gameObject.SetActive(true);
                starsChestSlider.value = starsChestSlider.maxValue;
            }
            else
            {
                starsChestCount.text = CurrencyController.StarsChestCount.ToString() + "/" + starsChestSlider.maxValue.ToString();
                starsChestRewardButton.gameObject.SetActive(false);
                starsChestSlider.value = CurrencyController.StarsChestCount;
            }
            if (CurrencyController.LevelsChestCount >= levelsChestSlider.maxValue)
            {
                levelsChestCount.text = "Open";
                levelsChestRewardButton.gameObject.SetActive(true);
                levelsChestSlider.value = levelsChestSlider.maxValue;
            }
            else
            {
                levelsChestCount.text = CurrencyController.LevelsChestCount.ToString() + "/" + levelsChestSlider.maxValue.ToString();
                levelsChestRewardButton.gameObject.SetActive(false);
                levelsChestSlider.value = CurrencyController.LevelsChestCount;
            }
        }
        public void FlyStarsToLevelsChest()
        {
            int difference = 0;

            if (starsChestSlider.value < CurrencyController.StarsChestCount && starsChestSlider.value != starsChestSlider.maxValue)
            {
                difference = Mathf.RoundToInt((starsChestSlider.value - CurrencyController.StarsChestCount));
                difference *= -1;
                CurrencyController.StarsChestCount -= difference;
            }
            else
            {
                difference = LevelController.Instance.CurrentLevelStars;
            }

            if (difference <= 0) return;

            Action UpdateView = delegate ()
            {
                CurrencyController.StarsChestCount++;
                UpdateMainMenuView();
            };
            Action UpdateViewWithMore20 = delegate ()
            {
                float value = CurrencyController.StarsChestCount;

                float to = value + difference;
                var ds = DOTween.To(() => (int)value, x => value = x, (int)to, 1).OnUpdate(() =>
                {

                    CurrencyController.StarsChestCount = (int)value;
                    UpdateMainMenuView();

                });
            };
            if (difference <= 10)
            {
                starsFly.SpawnCoins(difference, starsFly.center, starsChestTarget, difference);
                starsFly.OnCompleted += UpdateView;
            }
            else
            {
                starsFly.SpawnCoins(10, starsFly.center, starsChestTarget, difference);
                starsFly.OnCompletedSpawn += UpdateViewWithMore20;
            }
            if (TimeHeistController.isTimeHeistEventEnabled && CurrencyController.TimeHeistDiamondsToAdd > 0)
            {
                starsFly.OnCompletedSpawn += FlyDiamonds;

            }
            else
            {
                starsFly.OnCompletedSpawn += (() => { TimeHeistView.isGivingReward = false; });
            }
        }
        public void FlyCoinsToLevelsChest(int coinsCount)
        {
            coinsFly.StopAllCoroutines();

            SFXConfig.PlaySoundEffect(SFXConfig.coinsSpawn);

            Action UpdateViewWithMore20 = delegate ()
            {
                float value = CurrencyController.Coins;

                float to = value + coinsCount;

                var ds = DOTween.To(() => (int)value, x => value = x, (int)to, 1).OnUpdate(() =>
                {

                    CurrencyController.Coins = (int)value;
                    UpdateMainMenuView();

                });
            };

            coinsFly.SpawnCoins(10, coinsFly.center, coinsFlyTarget, coinsCount);
            coinsFly.OnCompletedSpawn += UpdateViewWithMore20;
            coinsFly.OnCompletedSpawn += (() => { TimeHeistView.isGivingReward = false; });
        }
        public void FlyHeartToLevelsChest(int heartsFreeTime)
        {
            heartsFly.StopAllCoroutines();

            heartsFly.SpawnCoins(1, heartsFly.center, heartFlyTarget, heartsFreeTime);
            heartsFly.OnCompleted += (() => 
            {
                TimeHeistView.isGivingReward = false;
                UpdateMainMenuView();
            });

        }
       public static Action UpdateViewWithMore20Diamonds = delegate ()
        {
            float duration = 1f;

            if (CurrencyController.TimeHeistDiamondsToAdd <= 10)
            {
               
                duration = 2;
            }
            else if (CurrencyController.TimeHeistDiamondsToAdd > 10 && (CurrencyController.TimeHeistDiamondsToAdd <= 20))
            {
              
                duration = 2f;
            }
            else
            {
              
                duration = 3.5f;
            }

            int startValue = CurrencyController.TimeHeistDiamonds;
            float value = CurrencyController.TimeHeistDiamonds;

            float to = value + CurrencyController.TimeHeistDiamondsToAdd;

            int startCurrentLevelDiomandValue = CurrencyController.TimeHeistDiamondsToAdd;


            diamondsTweener = DOTween.To(() => (int)value, x => value = x, (int)to, duration).OnUpdate(() =>
            {

                CurrencyController.TimeHeistDiamonds = (int)value;
                CurrencyController.TimeHeistDiamondsToAdd = (startValue - (int)value) + startCurrentLevelDiomandValue;

                TimeHeistView.UpdateTimeHeistViewEventHandler?.Invoke();


            });
        };
        public void FlyDiamonds()
        {
            diomandsFly.StopAllCoroutines();
            if (CurrencyController.TimeHeistDiamondsToAdd <= 0) return;

            if (CurrencyController.TimeHeistDiamondsToAdd <= 10)
            {
                diomandsFly.SpawnCoins(CurrencyController.TimeHeistDiamondsToAdd, diomandsFly.center, diamondsFlyTarget, CurrencyController.TimeHeistDiamondsToAdd);
            }
            else
            {
                diomandsFly.SpawnCoins(10, diomandsFly.center, diamondsFlyTarget, CurrencyController.TimeHeistDiamondsToAdd);
            }

            diomandsFly.OnCompletedSpawn += UpdateViewWithMore20Diamonds;
            diomandsFly.OnCompletedSpawn += (() => { TimeHeistView.isGivingReward = false; });
        }
        #region Chests

        public void FlyChestCoins(int coinsCount)
        {
            SFXConfig.PlaySoundEffect(SFXConfig.coinsSpawn);
            chestCoinsFly.StopAllCoroutines();

            Action UpdateViewWithMore20 = delegate ()
            {
                float value = CurrencyController.Coins;

                float to = value + coinsCount;

                var ds = DOTween.To(() => (int)value, x => value = x, (int)to, 1).OnUpdate(() =>
                {

                    CurrencyController.Coins = (int)value;
                    UpdateMainMenuView();

                });
            };

            chestCoinsFly.SpawnCoins(10, chestCoinsFly.center, coinsFlyTarget, coinsCount);
            chestCoinsFly.OnCompletedSpawn += UpdateViewWithMore20;
            chestCoinsFly.OnCompletedSpawn += (() => { TimeHeistView.isGivingReward = false; });
        }
        public void OpenLevelsChestRewardScreen()
        {
            if (!(CurrencyController.LevelsChestCount >= 10)) return;

            levelsChestScreen.gameObject.SetActive(true);

            TimeHeistView.isGivingReward = true;

            //levelsChestAnimator.Play("Opening", 0, 0f);
            levelsChestAnimator.speed = 0f;
            levelsChestAnimator.enabled = false;
            levelChestImage.sprite = levelsChestSprite;

            footerScreen.SetActive(false);
        }
        public void OpenStarsChestRewardScreen()
        {

            if (!(CurrencyController.StarsChestCount >= 1000)) return;

            starsChestScreen.gameObject.SetActive(true);

            TimeHeistView.isGivingReward = true;
            //starsChestAnimator.Play("Opening", 0, 0f);
            starsChestAnimator.speed = 0f;
            starsChestAnimator.enabled = false;
            starChestImage.sprite = starsChestSprite;

            footerScreen.SetActive(false);
        }
        public void LevelsChestRewardButtonPressed()
        {

            CurrencyController.LevelsChestCount -= 10;
            ChestsRewardsController.Instance.GenerateRewards(claimLevelsChestRewardButton, levelsChestScreen, levelsChestAnimator, levelsChestRewardAnimator, levelsChestRewardImage, this);
            UpdateMainMenuView();
            GameController.SaveGameData();
            startClaimLevelsChestRewardButton.gameObject.SetActive(false);
            levelView.UpdateRewardedChestsView();

            levelsChestAnimator.speed = 1f;


        }

        public void StarsChestRewardButtonPressed()
        {
            CurrencyController.StarsChestCount -= 1000;
            ChestsRewardsController.Instance.GenerateRewards(claimStarsChestRewardButton, starsChestScreen, starsChestAnimator, starsChestRewardAnimator, starsChestRewardImage, this);
            UpdateMainMenuView();
            GameController.SaveGameData();
            startClaimStarsChestRewardButton.gameObject.SetActive(false);
            levelView.UpdateRewardedChestsView();

            starsChestAnimator.speed = 1f;
        }
        private void ClaimLevelsChestRewardButtonPressed()
        {
            levelsChestScreen.SetActive(false);
            levelsChestRewardImage.gameObject.SetActive(false);
            startClaimLevelsChestRewardButton.gameObject.SetActive(true);
            claimLevelsChestRewardButton.gameObject.SetActive(false);

            FlyChestCoins(ChestsRewardsController.Instance.coinsCount);

            if (FooterView.isCanSwipe == true)
            {
                footerScreen.SetActive(true);
            }
        }
        private void ClaimStarsChestRewardButtonPressed()
        {
            starsChestScreen.SetActive(false);
            starsChestRewardImage.gameObject.SetActive(false);
            startClaimStarsChestRewardButton.gameObject.SetActive(true);
            claimStarsChestRewardButton.gameObject.SetActive(false);

            FlyChestCoins(ChestsRewardsController.Instance.coinsCount);

            if (FooterView.isCanSwipe == true)
            {
                footerScreen.SetActive(true);
            }
        }

        #endregion

    }
}