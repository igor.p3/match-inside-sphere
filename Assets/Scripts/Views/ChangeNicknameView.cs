using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Matchmania;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChangeNicknameView : MonoBehaviour
{
    [SerializeField] private TMP_InputField _inputField;
    [SerializeField] private TextMeshProUGUI textLengthWarning;
    [SerializeField] private float textFadeTime = 2f;
    [SerializeField] private GameObject changeNicknameScreen;
    [SerializeField] private Button closeButton;
    public static bool IsNicknameChanged = false;
    public event Action<string> OnNicknameChanged;

    #region Singleton
    private static ChangeNicknameView _instance;
    public static ChangeNicknameView Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<ChangeNicknameView>();
            }

            return _instance;
        }
    }
    #endregion

    void Start()
    {
        closeButton.onClick.AddListener(CloseButtonPressed);
    }
    private void ShowLengthWarning()
    {
        textLengthWarning.DOFade(1, 0f);
        textLengthWarning.gameObject.SetActive(true);
        textLengthWarning.DOFade(0, textFadeTime).OnComplete(() => textLengthWarning.gameObject.SetActive(false));
    }

    public void ShowNicknameChangeScreen()
    {
        if(IsNicknameChanged) return;
        LevelView.OpenScreen(changeNicknameScreen);
    }
    public void CloseButtonPressed()
    {
        LevelView.CloseScreen(changeNicknameScreen);
    }
    public void SubmitChangedNickname()
    {
        if (_inputField.text.Length < 4)
        {
            ShowLengthWarning();
            return;
        }

      LevelView.CloseScreen(changeNicknameScreen);

        // Changes the name to new name and send new name in the DB
        var oldName = GameController.playerName;
        GameController.playerName = _inputField.text;
        IsNicknameChanged = true;
        GameController.SaveGameData();
        LeaderboardController.SendPlayersScoreData(() =>
        {
            LeaderboardController.ChangePlayersNickname(oldName);
            OnNicknameChanged?.Invoke(oldName);
        }, () =>
        {
            LeaderboardController.ChangePlayersNickname(oldName);
            OnNicknameChanged?.Invoke(oldName);
            Debug.LogError("Couldn't send data to server in changed nickname, will try again later");
        });

}
}
