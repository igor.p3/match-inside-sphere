using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;
using Lean.Pool;
using System.Collections.Generic;
using DG.Tweening;

namespace Matchmania
{
    public class FakeLeaderboardView : MonoBehaviour
    {
        [SerializeField] private ScrollRect leaderBoardScrollRect;

        [Header("Prefabs")]

        [SerializeField] private FakeLeaderboardTabView playerPanel;
        [SerializeField] private FakeLeaderboardTabView npcPanelPrefab;

        [Header("Transforms")]

        [SerializeField] private Transform panelsParent;
        [SerializeField] private Transform outPanelParent;
        [SerializeField] private Transform leaderboardScreenView;

        [Header("Buttons")]

        [SerializeField] private Button scoreX2Button;
        [SerializeField] private Button regularScoreButton;

        [Header("Texts")]

        [SerializeField] private TextMeshProUGUI currentPlayerScoreText;
        [SerializeField] private TextMeshProUGUI bestPlayerScoreText;
        [SerializeField] private TextMeshProUGUI currentLevelIndexText;
        [SerializeField] private TextMeshProUGUI noThanksText;

        private List<FakeLeaderboardTabView> npcTabs = new List<FakeLeaderboardTabView>();

        public static bool isUpdating;

        void Start()
        {
            scoreX2Button.onClick.AddListener(ScoreX2ButtonPressed);
            regularScoreButton.onClick.AddListener(RegularScoreButtonPressed);

            FakeLeaderboardController.OpenAtLevelEndEventHandler += OpenFakeLeaderboardAtLevelEnd;

            StartCoroutine(CreateStartTabs());
        }

        private void ScoreX2ButtonPressed()
        {
            LevelView.Instance.OnFinishLevelClaimBonusRewardButtonPressed();
        }
        private void RegularScoreButtonPressed()
        {
            LevelView.Instance.OnFinishLevelClaimRewardButtonPressed();
        }
        private void SetTexts()
        {
            currentPlayerScoreText.text = LevelController.Instance.CurrentLevelStars.ToString();
            bestPlayerScoreText.text = FakeLeaderboardController.GetBestScore(LevelController.Instance.CurrentLevelStars).ToString();
            currentLevelIndexText.text = $"Level {LevelController.CurrentLevelIndex + 1}";
        }
        private void OpenFakeLeaderboardAtLevelEnd()
        {
            LevelView.OpenScreen(leaderboardScreenView.gameObject);
            leaderboardScreenView.GetComponent<RectTransform>().localPosition = Vector3.zero;
            SetTexts();

            currentPlayerScoreText.gameObject.SetActive(true);
            currentLevelIndexText.gameObject.SetActive(true);

            StartCoroutine(UpdateTabs());
        }
        IEnumerator UpdateTabs()
        {
            isUpdating = true;
            FakeLeaderboardFollowerVIew.smoothTime = 0f;
            leaderBoardScrollRect.enabled = false;
            playerPanel.transform.SetParent(outPanelParent);
            playerPanel.transform.DOScale(1.3f, 1);

            scoreX2Button.gameObject.SetActive(false);
            scoreX2Button.GetComponent<CanvasGroup>().alpha = 0;
            noThanksText.DOFade(0, 0);
            regularScoreButton.gameObject.SetActive(false);

            yield return new WaitForSeconds(1f);

            int npcScore = CurrencyController.FakeLeaderboardStars;
            CurrencyController.FakeLeaderboardStars += LevelController.Instance.CurrentLevelStars;

            FakeLeaderboardFollowerVIew.smoothTime = 0.05f;

            for (int i = 0; i < 10000; i++)
            {
                var nextAbovePlayer = panelsParent.transform.GetChild(4).GetComponent<FakeLeaderboardTabView>();
                var firstPlayer = panelsParent.transform.GetChild(0).GetComponent<FakeLeaderboardTabView>();

                if (nextAbovePlayer.score >= CurrencyController.FakeLeaderboardStars)
                {
                    break;
                }
              
                var lastTab = panelsParent.transform.GetChild(panelsParent.transform.childCount - 1).GetComponent<FakeLeaderboardTabView>();
                npcTabs.Remove(lastTab);
                Destroy(lastTab.gameObject);

                FakeLeaderboardTabView npcTab = Instantiate(npcPanelPrefab);

                if (firstPlayer.place != 1)
                {
                    npcTab.transform.SetParent(panelsParent);
                    npcTab.gameObject.SetActive(true);
                    npcTab.transform.localScale = Vector3.one;
                    npcTab.transform.localPosition = Vector3.zero;
                    npcScore += Random.Range(1, 5);
                    npcTab.score = npcScore;
                    npcTab.place = firstPlayer.place - 1;
                    npcTab.SetView();
                    npcTab.transform.SetAsFirstSibling();
                    npcTabs.Add(npcTab);
                }

                FakeLeaderboardController.playerPlace--;

                playerPanel.name = GameController.playerName.ToString();
                playerPanel.playerScoreText.text = CurrencyController.FakeLeaderboardStars.ToString();
                playerPanel.playerPlace.text = FakeLeaderboardController.playerPlace.ToString();

                if(FakeLeaderboardController.playerPlace == 1)
                {
                    break;
                }
                yield return new WaitForSeconds(0.05f);
            }

            //var tab = panelsParent.transform.GetChild(5).GetComponent<FakeLeaderboardTabView>();
            //npcTabs.Remove(tab);
            //Destroy(tab.gameObject);
            //playerPanel.transform.SetParent(panelsParent);
            //playerPanel.transform.SetSiblingIndex(5);

            playerPanel.transform.DOScale(1f, 1f);

            scoreX2Button.gameObject.SetActive(true);
            scoreX2Button.GetComponent<CanvasGroup>().DOFade(1, 0.5f);

            //leaderBoardScrollRect.enabled = true;
            GameController.SaveGameData();

            yield return new WaitForSeconds(1.5f);

            regularScoreButton.gameObject.SetActive(true);

            regularScoreButton.gameObject.SetActive(true);
            noThanksText.DOFade(1, 1f);

            FakeLeaderboardFollowerVIew.smoothTime = 0f;
            isUpdating = false;
        }

        private IEnumerator CreateStartTabs()
        {
            yield return null;

            int npcScore = CurrencyController.FakeLeaderboardStars + Random.Range(1, 2);
            int npcMinusScore = CurrencyController.FakeLeaderboardStars - Random.Range(1, 2);

            if (panelsParent.childCount <= 1)
            {
                if(FakeLeaderboardController.playerPlace == 0)
                {
                    FakeLeaderboardController.playerPlace = 15000;
                }
                for (int i = 0; i < 5; i++)
                {
                    FakeLeaderboardTabView npcTab = Instantiate(npcPanelPrefab);

                    npcTab.transform.SetParent(panelsParent);
                    npcTab.gameObject.SetActive(true);
                    npcTab.transform.localScale = Vector3.one;
                    npcTab.transform.localPosition = Vector3.zero;
                    npcScore += Random.Range(0, 2);
                    npcTab.score = npcScore;
                    npcTab.place = FakeLeaderboardController.playerPlace - 1 - i;
                    npcTab.SetView();
                    npcTab.transform.SetAsFirstSibling();

                    npcTabs.Add(npcTab);

                    playerPanel.name = GameController.playerName.ToString();
                    playerPanel.playerScoreText.text = CurrencyController.FakeLeaderboardStars.ToString();
                    playerPanel.flagImage.sprite = LeaderBoardView.GetFlag(RegionIdentifier.savedCountryName);
                    playerPanel.playerPlace.text = FakeLeaderboardController.playerPlace.ToString();
                }
                for (int i = 0; i < 5; i++)
                {
                    FakeLeaderboardTabView npcTab = Instantiate(npcPanelPrefab);

                    npcTab.transform.SetParent(panelsParent);
                    npcTab.gameObject.SetActive(true);
                    npcTab.transform.localScale = Vector3.one;
                    npcTab.transform.localPosition = Vector3.zero;

                    npcTab.place = FakeLeaderboardController.playerPlace + 1 + i;

                    npcMinusScore -= Random.Range(1, 2);
                    if (npcMinusScore > 0)
                    {
                        npcTab.score = npcMinusScore;
                    }
                    else
                    {
                        npcTab.score = 0;
                    }
                    npcTab.SetView();

                    npcTab.playerNameText.text = FakeLeaderboardController.NameGenerator();
                    npcTab.flagImage.sprite = FakeLeaderboardController.GenerateCountry();
                    npcTab.playerPlace.text = (FakeLeaderboardController.playerPlace + 1 + i).ToString();

                    npcTabs.Add(npcTab);

                    playerPanel.name = GameController.playerName.ToString();
                    playerPanel.playerScoreText.text = CurrencyController.FakeLeaderboardStars.ToString();
                    playerPanel.flagImage.sprite = LeaderBoardView.GetFlag(RegionIdentifier.region.EnglishName.ToString());
                    playerPanel.playerPlace.text = FakeLeaderboardController.playerPlace.ToString();
                }
            }
            //playerPanel.transform.SetParent(panelsParent);
            //playerPanel.transform.SetSiblingIndex(5);
        }
    }
}