﻿using System.Collections.Generic;

namespace Matchmania
{
    [System.Serializable]
    public class LevelModel
    {
        public string hardness;
        public float timer;
        public int itemsCount;
        public string blocker_01;
        public string blocker_02;
        public string blocker_03;

    }
}
