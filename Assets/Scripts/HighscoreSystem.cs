﻿namespace HyperCasualTemplate
{
    public static class HighscoreSystem
    {
        public static int Highscore { get; private set; }

        public static void UpdateHighscore(int value)
        {
            if (Highscore < value) Highscore = value;
        }
    }
}