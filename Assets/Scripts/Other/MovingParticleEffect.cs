using System.Collections;
using DG.Tweening;
using Matchmania;
using UnityEngine;
using UnityEngine.UI;

public class MovingParticleEffect : MonoBehaviour
{
    [SerializeField] private GameObject particleObject;
    [SerializeField] private Image particleImage;
    
    [SerializeField] private float movementDuration = 0.75f;
    [SerializeField] private float startMovementDuration = .25f;
    [SerializeField] private float shrinkDuration = .25f;
    [SerializeField] private Ease easeType = Ease.InOutQuint;
    
    [SerializeField] private RectTransform targetTransform;
    private Vector3 _targetPosition;
    [SerializeField] private float particleOffset; 

    void Start()
    {
        _targetPosition = targetTransform.position;
    }

    public void ChangeSprite(Sprite newSprite)
    {
        particleImage.sprite = newSprite;
    }

    public void SetTarget(RectTransform target)
    {
        targetTransform = target;
    }

    public void StartAnimation()
    {
        StartCoroutine(MoveToTargetAnimaiton());
    }
    
    IEnumerator MoveToTargetAnimaiton()
    {
        yield return new WaitForSeconds(0.5f);
        Sequence animationSequence = DOTween.Sequence();
        var randomVector2WOffset = transform.position + new Vector3(Random.Range(-particleOffset, particleOffset),
            Random.Range(-particleOffset, particleOffset));
        animationSequence.Append(transform.DOMove(randomVector2WOffset, startMovementDuration).SetEase(easeType));
        
        animationSequence.Append(transform.DOMove(_targetPosition, movementDuration).SetEase(easeType)
            .OnComplete(() => transform.position = _targetPosition));
        
        animationSequence.Append(transform.DOScale(0, shrinkDuration).SetEase(easeType));
        
        animationSequence.OnComplete(() =>
        {
            particleObject.SetActive(false);
        });
    }
}
