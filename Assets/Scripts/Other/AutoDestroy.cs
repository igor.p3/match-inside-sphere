﻿using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField] private float timeToDestroy = 0.5f;
    void Start()
    {
        Destroy(gameObject, timeToDestroy);
    }
}
