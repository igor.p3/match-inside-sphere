using System;
using System.Collections;
using System.Collections.Generic;
using GameAnalyticsSDK.Setup;
using Matchmania;
using UnityEngine;
using VoxelBusters.CoreLibrary;
using VoxelBusters.EssentialKit;


public class CloudSavingController : MonoBehaviour
{
     #region Singleton
        private static CloudSavingController _instance;
        public static CloudSavingController Instance
        {
            get
            {
                if (_instance == null) _instance = FindObjectOfType<CloudSavingController>();

                return _instance;
            }
        }
        #endregion

    private static string savedDataKey = "allSavedData";
    public static bool IsAuthenticated;
    public static ILocalPlayer LocalPlayer;
    [SerializeField] private CloudSavingView _cloudSavingView;
    private void Start()
    {
        IsAuthenticated = GameServices.IsAuthenticated;
        if (IsAuthenticated)
        {
            CloudServices.Synchronize();
            return;
        }
        
        if(GameController.isFirstSession == false) return;
        
        if (GameServices.IsAvailable())
        {
            GameServices.Authenticate();
        }
    }
    private void OnEnable()
        {
            // register for events
            CloudServices.OnUserChange              += OnUserChange;
            CloudServices.OnSavedDataChange         += OnSavedDataChange;
            CloudServices.OnSynchronizeComplete     += OnSynchronizeComplete;
            GameServices.OnAuthStatusChange         += OnAuthStatusChange;

        }

    private void OnDisable()
    {
            // unregister from events
            CloudServices.OnUserChange              -= OnUserChange;
            CloudServices.OnSavedDataChange         -= OnSavedDataChange;
            CloudServices.OnSynchronizeComplete     -= OnSynchronizeComplete;
            GameServices.OnAuthStatusChange         -= OnAuthStatusChange;

    }

    private void OnAuthStatusChange(GameServicesAuthStatusChangeResult result, Error error)
    {
        if (error == null)
        {
            //Debug.Log("Received auth status change event");
            //Debug.Log("Auth status: " + result.AuthStatus);
            //Debug.Log("Local player: " + result.LocalPlayer);
            switch (result.AuthStatus)
            {
                case LocalPlayerAuthStatus.Authenticated:
                    IsAuthenticated = true;
                    LocalPlayer = result.LocalPlayer;
                    GameController.playerName = result.LocalPlayer.DisplayName;
                    GameController.SaveGameData();
                    break;
                case LocalPlayerAuthStatus.NotAvailable:
                    IsAuthenticated = false;
                    break;
            }

            _cloudSavingView.UpdateButtons();
        }
        else
        {
            Debug.LogError("Failed login with error : " + error);
            IsAuthenticated = false;
        }
    }

    private void OnSynchronizeComplete(CloudServicesSynchronizeResult result)
        {
            //Debug.Log("Received synchronize finish callback.");
            //Debug.Log("Status: " + result.Success);
            // By this time, you have the latest data from cloud and you can start reading.
            
        }

        private void OnSavedDataChange(CloudServicesSavedDataChangeResult result)
        {
            GameController.LoadGameData(LoadData());
            //throw new NotImplementedException();
        }

        private void OnUserChange(CloudServicesUserChangeResult result, Error error)
        {
            //throw new NotImplementedException();
        }

        public static void SaveDataToCloud(SaveDataModel saveDataModel)
        {
            if (IsAuthenticated == false) return;
            IJsonServiceProvider serializer = new DefaultJsonServiceProvider();
            string json = JsonUtility.ToJson(saveDataModel);
            
            CloudServices.SetString(savedDataKey, json);
            CloudServices.Synchronize();
        }

        public static SaveDataModel LoadData()
        {
            IJsonServiceProvider serializer = new DefaultJsonServiceProvider();
            var jsonString = CloudServices.GetString(savedDataKey);
            if (jsonString == null)
            {
                Debug.LogError("Couldn't read any data from cloud. Must be not synced yet?");
            }
            var savedDataFromCloud = JsonUtility.FromJson<SaveDataModel>(jsonString);
            return savedDataFromCloud;
        }

        public static void LoadProgressButton()
        {
            GameController.LoadGameData(LoadData());
            Debug.LogWarning("Loaded new data from cloud");
        }

        public void SignIn()
        {
            GameServices.Authenticate();
            _cloudSavingView.UpdateButtons();
        }

        public void SignOut()
        {
            GameServices.Signout();
            _cloudSavingView.UpdateButtons();
        }
}
