using System;
using System.Collections;
using System.Collections.Generic;
using Matchmania;
using UnityEngine;
using Random = UnityEngine.Random;

public class HardLevelsDialogueController : MonoBehaviour
{
    [SerializeField] private HardLevelDialoguesView _hardLevelDialoguesView;
    [SerializeField] private MainMenuView _mainMenuView;

    private void Start()
    {
        _mainMenuView.OnMenuStartLevelButtonPressed += DetermineIfLevelHard;
        _mainMenuView.OnMenuStartLevelButtonPressed += UpdateAllLevelsInDialogues;
        LevelController.OnLevelNumChanged += (num) => UpdateAllLevelsInDialogues();
        LevelView.UpdateLevelIndexTexts += UpdateAllLevelsInDialogues;
        UpdateAllLevelsInDialogues();
    }

    private void UpdateAllLevelsInDialogues()
    {
        _hardLevelDialoguesView.UpdateAllLevelNumbers(LevelController.CurrentLevelIndex);
    }
    private void DetermineIfLevelHard()
    {
        // Algorithm to determine if it's hard.
        // If not hard - return

        // If hard - change windows layout

        //todo remove this 
        // if (Random.Range(0f, 1f) > 0.5f)
        // {
        //     _hardLevelDialoguesView.SwitchToHard();
        // }
        // else
        // {
        //     _hardLevelDialoguesView.SwitchToNormal();
        // }
        _hardLevelDialoguesView.SwitchToNormal();
    }
    
}
