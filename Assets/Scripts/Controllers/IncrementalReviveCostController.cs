using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncrementalReviveCostController
{
    private readonly int _initialPrice;
    public int CurrentPrice { get; private set; }
    private readonly int _increaseRate;
    private int _maxPrice;

    public IncrementalReviveCostController(int initialPrice = 900, int increaseRate = 1000, int maxPrice = 2900)
    {
        _initialPrice = initialPrice;
        _increaseRate = increaseRate;
        _maxPrice = maxPrice;
        CurrentPrice = _initialPrice;
    }

    public void IncreasePrice()
    {
        CurrentPrice += _increaseRate;
        CurrentPrice = Mathf.Clamp(CurrentPrice, 0, _maxPrice);
    }

    public void ResetPrice()
    {
        CurrentPrice = _initialPrice;
    }
}
