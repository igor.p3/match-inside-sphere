﻿using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using System.Linq;
using Lean.Pool;
using System;

namespace Matchmania
{
    public class ItemsSpawnController : MonoBehaviour
    {
        #region Singleton
        private static ItemsSpawnController _instance;
        public static ItemsSpawnController Instance
        {
            get
            {
                if (_instance == null)
                {
                    _instance = GameObject.FindObjectOfType<ItemsSpawnController>();
                }

                return _instance;
            }
        }
        #endregion

        [SerializeField] private LevelView levelView;
        [SerializeField] private SFXConfig SFXConfig;

        [SerializeField] private List<ItemController> specialItemsForCreatives = new List<ItemController>();

        private bool isSpawningItems;


        public void SpawnItems(Transform parent, bool isTutorial, Transform[] tutorialLevelPos)
        {
            if (isSpawningItems == false)
            {
                StartCoroutine(_SpawnItems(parent, isTutorial, tutorialLevelPos));
            }
        }
        public IEnumerator _SpawnItems(Transform parent, bool isTutorial, Transform[] tutorialLevelPos)
        {
            isSpawningItems = true;
            yield return null;
            SceneLoadingController.UpdateUnlockedItems();
            LevelController.Instance.IsCanMakeTurn = false;
            List<ItemController> levelItems = new List<ItemController>();
            List<ItemController> lastUnlockedItems = new List<ItemController>();
            GravityAttractorController planet = GameObject.FindGameObjectWithTag("Planet").GetComponent<GravityAttractorController>();
            LevelController.Instance.IsPlayingLevel = false;

            List<ItemController> itemsToSpawn = new List<ItemController>();
            int unlockedItemsToSpawnCount = GameController.lastUnlockedItems.Count;

            if(LevelController.CurrentLevelIndex <= 4)
            {
                levelItems = Extensions.GetRandomElements(GameController.unlockedItems, LevelController.Instance.CurrentLevelModel.itemsCount);
            }
            else
            {
                levelItems = Extensions.GetRandomElements(GameController.unlockedItems, LevelController.Instance.CurrentLevelModel.itemsCount - unlockedItemsToSpawnCount);
                lastUnlockedItems = Extensions.GetRandomElements(GameController.lastUnlockedItems, unlockedItemsToSpawnCount);
            }

            for (int i = 0; i < levelItems.Count; i++)
            {
                for (int j = 0; j < GameModel.itemMatchCount; j++)
                {
                    itemsToSpawn.Add(levelItems[i]);
                }
            }
            for (int i = 0; i < lastUnlockedItems.Count; i++)
            {
                for (int j = 0; j < GameModel.itemMatchCount; j++)
                {
                    itemsToSpawn.Add(lastUnlockedItems[i]);
                }
            }

            // Mix items in list
            System.Random RND = new System.Random();
            for (int i = 0; i < itemsToSpawn.Count; i++)
            {
                var tmp = itemsToSpawn[0];
                itemsToSpawn.RemoveAt(0);
                itemsToSpawn.Insert(RND.Next(itemsToSpawn.Count), tmp);
            }
            //Spawn items

            //ItemsForCreatives
            //itemsToSpawn = specialItemsForCreatives;


            for (int i = 0; i < itemsToSpawn.Count; i++)
            {
                //Vector3 randomRotation = new Vector3(Random.Range(0, 360), Random.Range(0, 360), Random.Range(0, 360));

                //Quaternion rotation = Quaternion.Euler(randomRotation);
                var itemByName = itemsToSpawn[i];
                if (itemByName == null) continue;
                ItemController item = LeanPool.Spawn(itemByName, LevelController.Instance.sphereCenter.position, Quaternion.identity, parent);
                LevelController.Instance.CurrentLevelItems.Add(item);

                item.transform.rotation = Quaternion.Euler(item.slotRotation);
                item.isRotate = false;
           
                //item.transform.localScale = new Vector3(item.normalScale, item.normalScale, item.normalScale) / 1.5f;
                item.transform.localScale = new Vector3(0.01f, 0.01f, 0.01f);

                item.collider.enabled = true;

                item.rigidbody.isKinematic = false;
                item.isMovingToBar = false;
                item.outline.enabled = false;
                item.transform.DOScale(item.normalScale, 1);
                if (SettingsController.isVibrationOn == true)
                    MMVibrationManager.Haptic(HapticTypes.LightImpact, false, true, this);
                //yield return new WaitForSeconds(0.02f);
                item.planet = planet;
                item.transform.SetParent(item.planet.transform);

            }
            //GravityAttractorController.DissoleItems(1, 0, 2);

            GravityAttractorController.animator.transform.GetChild(0).gameObject.SetActive(true);
         
            GravityAttractorController.animator.enabled = true;
            GravityAttractorController.animator.Play("PlanetRotation", 0, 0);


            yield return new WaitForSeconds(2f);

            GravityAttractorController.animator.enabled = false;

            LevelController.Instance.IsPlayingLevel = true;
            LevelController.Instance.IsCanMakeTurn = true;
            isSpawningItems = false;
        }
        public void PlayHoseSoundEffect()
        {
            SFXConfig.PlaySoundEffect(SFXConfig.hose);
        }
        public void DespawnItems()
        {
            for (int i = 0; i < LevelController.Instance.CurrentLevelItems.Count; i++)
            {
                LeanPool.Despawn(LevelController.Instance.CurrentLevelItems[i].gameObject);
            }
        }
        Vector3 RandomPointOnXZCircle(Vector3 center, float radius)
        {
            float angle = UnityEngine.Random.Range(0, 2f * Mathf.PI);
            Debug.Log("Cos " + Mathf.Cos(angle * radius));
            Debug.Log("Sin " + Mathf.Sin(angle * radius));
            return center + new Vector3(Mathf.Cos(angle), UnityEngine.Random.Range(-1f,1f), Mathf.Sin(angle)) * radius;
        }

        public static Vector3 GetPointOnUnitSphereCap(Quaternion targetDirection, float angle)
        {
            var angleInRad = UnityEngine.Random.Range(0.0f, angle) * Mathf.Deg2Rad;
            var PointOnCircle = (UnityEngine.Random.insideUnitCircle.normalized) * Mathf.Sin(angleInRad);
            Vector3 V = new Vector3(PointOnCircle.x, PointOnCircle.y, Mathf.Cos(angleInRad));
            return targetDirection * V;
        }
        public static Vector3 GetPointOnUnitSphereCap(Vector3 targetDirection, float angle)
        {
            return GetPointOnUnitSphereCap(Quaternion.LookRotation(targetDirection), angle);
        }
    }
}