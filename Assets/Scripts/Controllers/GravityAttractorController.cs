using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;
using Matchmania;

public class GravityAttractorController : MonoBehaviour
{
    private float rotationSpeed = 30;
    public float gravity = -10f;
    private float currentMouseX;
    private float currentMouseY;
    private float startTime;
    private float dragTreshold = 0.15f;

    public static MeshRenderer meshRenderer;
    private static Transform thisTransform;
    public static Animator animator;

    private void Start()
    {
        meshRenderer = transform.GetChild(0).GetComponent<MeshRenderer>();
        thisTransform = transform;
        animator = GetComponent<Animator>();
    }
    public void Attract(Transform body, Rigidbody rigidbody, ItemController itemController)
    {
        //Vector3 targetDirection = (body.position - transform.position).normalized;
        //Vector3 bodyUp = body.up;


        //body.rotation = Quaternion.FromToRotation(bodyUp, targetDirection) * body.rotation;
        //rigidbody.AddForce(targetDirection * gravity);
    }
    void Update()
    {
        RotateShape();
    }
    //public static void Dissolve(float startValue, float targetvalue, float duration, MeshRenderer meshRenderer)
    //{
    //    Material[] mats = meshRenderer.materials;

    //    float value = startValue;

    //    DOTween.To(() => value, x => value = x, targetvalue, duration).OnUpdate(() =>
    //    {
    //        mats[0].SetFloat("_Cutoff", value);
    //        meshRenderer.materials = mats;
    //    });
    //}

    //public static void DissoleItems(float startValue, float targetvalue, float duration)
    //{
    //    //MeshRenderer[] renderes = thisTransform.GetComponentsInChildren<MeshRenderer>();

    //    Dissolve(startValue, targetvalue, duration, meshRenderer);

    //    //foreach (var renderer in renderes)
    //    //{
    //    //    Dissolve(startValue, targetvalue, duration, renderer);
    //    //}
    //}
    private void RotateShape()
    {

        if (Input.touchCount == 1 && LevelController.Instance.IsPlayingLevel)
        {

            var touch = Input.GetTouch(0);

            if (touch.phase == TouchPhase.Began)
            {
                startTime = Time.time;
            }
            if (touch.phase == TouchPhase.Moved)
            {
                float timeDifference = Time.time - startTime;

                if (timeDifference > dragTreshold)
                {
                    currentMouseX = touch.deltaPosition.x;
                    currentMouseY = touch.deltaPosition.y;
                    transform.Rotate(currentMouseY * (rotationSpeed * Time.deltaTime), 0, -currentMouseX * (rotationSpeed * Time.deltaTime), Space.World);
                    LevelController.Instance.IsCanMakeTurn = false;
                }
            }
            if (touch.phase == TouchPhase.Ended)
            {
                LevelController.Instance.IsCanMakeTurn = true;
            }
        }
    }
}
