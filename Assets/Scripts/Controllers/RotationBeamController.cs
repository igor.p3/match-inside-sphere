using UnityEngine;

public class RotationBeamController : Obstacle
{
    public float rotationSpeed;
    [SerializeField] private Vector3 localStartPosition;

    public override void SetPosition()
    {
        transform.localPosition = localStartPosition;
    }
    void Update()
    {
        transform.Rotate(0, rotationSpeed * Time.deltaTime, 0);
    }
}
