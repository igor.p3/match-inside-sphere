﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.UI;
using Lean.Pool;
using System.Linq;
using System.Collections.Generic;

namespace Matchmania
{
    public class SceneLoadingController : MonoBehaviour
    {
        [SerializeField] private Slider loadingSlider;

        void Start()
        {
            GameModel.LoadlevelConfig(this);
            GameModel.LoadItemsProgressionConfig(this);
            GameModel.LoadTimeHeistProgressionConfig(this);
            GameModel.LoadDiomandsGainsProgressionConfig(this);
            StartCoroutine(LoadScene());
        }
        public IEnumerator LoadScene()
        {
            UpdateUnlockedItems();

   

            List<ItemController> itemsInPool = new List<ItemController>();

            foreach (var item in GameController.unlockedItems)
            {
                itemsInPool.Add(item);
            }
            foreach (var item in GameController.lastUnlockedItems)
            {
                itemsInPool.Add(item);
            }
            loadingSlider.maxValue = GameController.unlockedItems.Count;
            int middleValue = GameController.unlockedItems.Count / 2;
            loadingSlider.value = middleValue;

            for (int i = 0; i < itemsInPool.Count; i++)
            {
                var item_01 = LeanPool.Spawn(GameController.items[i]);
                var item_02 = LeanPool.Spawn(GameController.items[i]);
                var item_03 = LeanPool.Spawn(GameController.items[i]);

                if (i % 6 == 0 && i >= middleValue)
                {
                    loadingSlider.value = i;
                    yield return null;
                }
                LeanPool.Despawn(item_01);
                LeanPool.Despawn(item_02);
                LeanPool.Despawn(item_03);


            }
            loadingSlider.value = loadingSlider.maxValue;
            yield return new WaitUntil(() => GameModel.loadingIndex == 4);

            UpdateUnlockedItems();

            SceneManager.LoadScene(1);
        }

        public static void UpdateUnlockedItems()
        {
            GameController.unlockedItems.Clear();

            foreach (var item in GameModel.Items.Items)
            {
                if (LevelController.CurrentLevelIndex >= item.levelComplete)
                {
                    GameController.lastUnlockedItems.Clear();

                    foreach (var unlockedItem in item.progressionItems)
                    {
                        GameController.unlockedItems.Add(LevelController.GetItemByName(unlockedItem));
                        GameController.lastUnlockedItems.Add(LevelController.GetItemByName(unlockedItem));
                    }
                }
                else break;
            }
            if (LevelController.CurrentLevelIndex >= 5)
            {
                for (int i = 0; i < GameController.unlockedItems.Count; i++)
                {
                    for (int j = 0; j < GameController.lastUnlockedItems.Count; j++)
                    {
                        if (GameController.unlockedItems[i].name == GameController.lastUnlockedItems[j].name)
                        {
                            GameController.unlockedItems.Remove(GameController.unlockedItems[i]);
                        }
                    }
                }
            }
            GameController.unlockedItems.Distinct();
        }
    }
}