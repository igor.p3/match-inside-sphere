using UnityEngine;
using System;
using HyperCasualTemplate;
using UnityEngine.Purchasing;
using DG.Tweening;
using System.Collections;

namespace Matchmania
{
    public class IAPController : MonoBehaviour
    {
        public static Action OnPurchaseEventHandler;

        public GameObject removeAdsSlot;
        public GameObject removeAdsButton;
        public GameObject removeStarterBundle;
        public GameObject removeAdsDialog;
        public GameObject[] removeAdsIcons;

        public static bool isStarterBundlePurchased;

        IExtensionProvider extensions;

        void Start()
        {
            OnPurchaseEventHandler += GameController.SaveGameData;
            OnPurchaseEventHandler += () => { AnalyticsEvents.ExecuteEvent($"Purchase_{LevelController.CurrentLevelIndex + 1}"); };
            CheckIfAdsRemoved();
            CheckIfStarterBundleBought();

            RestorePurchases();
        }
        private void CheckIfAdsRemoved()
        {
            if (AdvertisementSystem.isAdsRemoved == true)
            {
                removeAdsButton.transform.DOMove(Vector3.right * 1000, 0.5f);
                removeAdsSlot.transform.parent = null;

                foreach (var icon in removeAdsIcons)
                {
                    icon.SetActive(false);
                }
                AdvertisementSystem.HideBanner();
            }
        }
        private void CheckIfStarterBundleBought()
        {
            if (isStarterBundlePurchased)
            {
                removeStarterBundle.SetActive(false);
            }
        }
        public void RemoveStarterBundle()
        {
            removeStarterBundle.transform.SetAsLastSibling();
            removeStarterBundle.GetComponent<CanvasGroup>().alpha = 0;
        }
        public void RestorePurchases()
        {
            //extensions.GetExtension<IAppleExtensions>().RestoreTransactions(result =>
            //{
            //    if (result)
            //    {
            //        // This does not mean anything was restored,
            //        // merely that the restoration process succeeded.
            //    }
            //    else
            //    {
            //        // Restoration failed.
            //    }
            //});

            //var productDetails = extensions.GetExtension<IAppleExtensions>().GetProductDetails();
        }
        public void RemoveAdsBundle()
        {
            AdvertisementSystem.isAdsRemoved = true;

            OnPurchaseEventHandler?.Invoke();
            CheckIfAdsRemoved();
            GameController.SaveGameData();
            LevelView.CloseScreen(removeAdsDialog);

            StartCoroutine(CloseRemoveAdsDialog());
            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 4.99);
          
        }
        IEnumerator CloseRemoveAdsDialog()
        {
            yield return new WaitForSeconds(0.6f);
            removeAdsDialog.SetActive(false);
        }
        
        public void SpecialOffer()
        {
            CurrencyController.Hints += 5;
            CurrencyController.Coins += 10000;
            HeartsController.Instance.SwitchToUnlimited(3600);

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();
            SpecialOfferController.Instance.HideOfferBought();
            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 4.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }

        public void CasualSpecialOffer()
        {
            CurrencyController.Hints += 5;
            CurrencyController.Coins += 15000;
  
            HeartsController.Instance.SwitchToUnlimited(3600);

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();
            SpecialOfferController.Instance.HideOfferBought();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 14.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }

        public void ExclusiveSpecialOffer()
        {
            CurrencyController.Hints += 5;
            CurrencyController.Coins += 20000;
            HeartsController.Instance.SwitchToUnlimited(3600);

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();
            SpecialOfferController.Instance.HideOfferBought();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 24.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }
        public void StarterBundle()
        {
            CurrencyController.Hints += 5;
            CurrencyController.Fans += 5;
            CurrencyController.Coins += 5000;

            HeartsController.Instance.SwitchToUnlimited(1800);
            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            isStarterBundlePurchased = true;
            OnPurchaseEventHandler?.Invoke();
            AdvertisementSystem.isCanShowInactiveAdd = true;
            GameController.SaveGameData();

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 4.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }
        public void PremiumBundle()
        {
            CurrencyController.Hints += 15;
            CurrencyController.Fans += 15;
            CurrencyController.Coins += 15000;
            HeartsController.Instance.SwitchToUnlimited(7200);
            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 29.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }
        public void EpicBundle()
        {
            CurrencyController.Hints += 25;
            CurrencyController.Fans += 25;
            CurrencyController.Coins += 40000;
            HeartsController.Instance.SwitchToUnlimited(43200); 
            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 49.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }
        public void MegaBundle()
        {
            CurrencyController.Hints += 10;
            CurrencyController.Fans += 10;
            CurrencyController.Coins += 5000;
            HeartsController.Instance.SwitchToUnlimited(3600);
            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 14.99);

            FanController.Instance.FanUsedEventHandler?.Invoke();
        }
        public void Coins_100()
        {
            CurrencyController.Coins += 1000;

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 1.99);
        }
        public void Coins_500()
        {
            CurrencyController.Coins += 5000;

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 4.99);
        }
        public void Coins_1000()
        {
            CurrencyController.Coins += 10000;

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 19.99);
        }
        public void Coins_2000()
        {
            CurrencyController.Coins += 20000;

            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 39.99);
        }
        public void Coins_5000()
        {
            CurrencyController.Coins += 50000;
            CurrencyController.OnChangeCurrencyEventHandler?.Invoke();
            OnPurchaseEventHandler?.Invoke();

            AdvertisementSystem.isCanShowInactiveAdd = true;

            AnalyticsEvents.ExecuteRevenuetEvent("at9937", 59.99);
        }
    }
}
