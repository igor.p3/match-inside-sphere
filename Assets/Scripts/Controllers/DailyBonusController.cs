using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Matchmania
{
    public class DailyBonusController : MonoBehaviour
    {
        public static Action OnDailyRewardOpenedEvent;

        public static string stringLastRewardDate;


        void Start()
        {
            CheckDailyBonus();
        }

        public static void CheckDailyBonus()
        {
            if(LevelController.CurrentLevelIndex >= 4 && !GameController.isFirstSession)
            {
                CheckDailyRewardTime();
            }
        }
        public  static void CheckDailyRewardTime()
        {
            if (stringLastRewardDate == string.Empty || stringLastRewardDate == "" || stringLastRewardDate == null)
            {
                OnDailyRewardOpenedEvent?.Invoke();
            }
            else
            {
                DateTime oldDate = Convert.ToDateTime(stringLastRewardDate);
                DateTime newDate = DateTime.Now;
                Debug.Log("LastDay: " + oldDate);
                Debug.Log("CurrDay: " + newDate);

                TimeSpan difference = newDate.Subtract(oldDate);
                if (difference.Days >= 1)
                {
                    OnDailyRewardOpenedEvent?.Invoke();
                    GameController.isTookFirstDailyReward = true;
                    GameController.SaveGameData();
                }
            }
       
        }

    }
}