using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Matchmania;
using UnityEngine;

[RequireComponent(typeof(TTEventDataModel))]
public class TTEventController : MonoBehaviour
{
    #region Singleton
    private static TTEventController _instance;
    public static TTEventController Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<TTEventController>();
            }

            return _instance;
        }
    }

    #endregion

    public static TTEventDataModel.TTEventName currentEvent;
    [Header("Timer related")]
    private Timer ttEventTimer;
    public static string TtEventSavedStartTime;
    public static DateTime TtStartTime;
    [SerializeField] private TTEventView _ttEventView;
    private TTEventDataModel _ttEventDataModel;
    private int tokensCollected;
    public int TokensCollected
    {
        get => tokensCollected;
        set
        {
            collectedTokensToShow += value - tokensCollected;
            tokensCollected = value;
            //_ttEventView.UpdateProgress(value,_ttEventDataModel.stepsToMake[CurrentTier]);
            CurrencyController.TTTokens = value;
            //CheckTokensAwards();
            GameController.SaveGameData();
        } 
    }
    private static float timeForTimer = 172800; // 48 hours
    public static int CurrentTier = 0;
    private int collectedTokensToShow;
    private int levelToUnlock = 15;
    private void Start()
    {
        _ttEventDataModel = GetComponent<TTEventDataModel>();
        tokensCollected = CurrencyController.TTTokens;
        
        if (currentEvent == TTEventDataModel.TTEventName.NeverStarted)
        {
            FirstEventStartCheck();
        }
        else
        {
            RealTimeCheck();
        }
        ShowDependingOnEvent();
        LevelController.OnLevelNumChanged += (i) => FirstEventStartCheck();
        UpdateTierAwards();
        _ttEventView.UpdateProgress(TokensCollected, _ttEventDataModel.stepsToMake[CurrentTier]);
        LevelView.GoHomeEventHandler += UpdateOpenedMainMenu;
    }

    private void UpdateTierAwards()
    {
        _ttEventView.AssignLoot(_ttEventDataModel.LootTable[CurrentTier].lootType,
            _ttEventDataModel.LootTable[CurrentTier].amountOfLoot);
    }

    public void CheckTokensAwards()
    {
        if (currentEvent == TTEventDataModel.TTEventName.NeverStarted ||
            currentEvent == TTEventDataModel.TTEventName.PostGodsEscapePause ||
            currentEvent == TTEventDataModel.TTEventName.PostSummerBreezePause ||
            currentEvent == TTEventDataModel.TTEventName.PostZooRamaPause)
        {
            return;
        }
        if (TokensCollected < _ttEventDataModel.stepsToMake[CurrentTier]) return;
        
        // Get the reward
        GiveTheReward(CurrentTier);
        TokensCollected -= _ttEventDataModel.stepsToMake[CurrentTier];
        if (CurrentTier == _ttEventDataModel.stepsToMake.Count - 1)
        {
            CurrentTier--;
        }
        else
        {
            CurrentTier++;
        }
        // Change the tier icon and amount according to loot table
        UpdateTierAwards();
        _ttEventView.UpdateProgress(TokensCollected, _ttEventDataModel.stepsToMake[CurrentTier]);
        GameController.SaveGameData();
    }

    public void UpdateOpenedMainMenu()
    {
        _ttEventView.UpdateProgress(TokensCollected, _ttEventDataModel.stepsToMake[CurrentTier]);
        switch (currentEvent)
        {
            case TTEventDataModel.TTEventName.ZooRama:
                _ttEventView.SpawnRewardsVFX(ParticleSpawner.ParticleObjects.ZooRamaToken, collectedTokensToShow);
                break;
            case TTEventDataModel.TTEventName.SummerBreeze:
                _ttEventView.SpawnRewardsVFX(ParticleSpawner.ParticleObjects.SummerBreezeToken, collectedTokensToShow);
                break;
            case TTEventDataModel.TTEventName.GodsEscape:
                _ttEventView.SpawnRewardsVFX(ParticleSpawner.ParticleObjects.GodsEscapeToken, collectedTokensToShow);
                break;
        }
        collectedTokensToShow = 0;
    }

    private void GiveTheReward(int tierWon)
    {
        var lootGot = _ttEventDataModel.LootTable[CurrentTier];
        switch (lootGot.lootType)
        {
            case TTEventDataModel.LootType.Heart:
                HeartsController.Instance.SwitchToUnlimited(lootGot.amountOfLoot);
                _ttEventView.SpawnRewardsVFX(ParticleSpawner.ParticleObjects.Hearts, 1);
                break;
            case TTEventDataModel.LootType.Hint:
                CurrencyController.Hints += lootGot.amountOfLoot;
                _ttEventView.SpawnRewardsVFX(ParticleSpawner.ParticleObjects.Hints, lootGot.amountOfLoot);
                break;
            case TTEventDataModel.LootType.Fan:
                CurrencyController.Fans += lootGot.amountOfLoot;
                break;
            case TTEventDataModel.LootType.Coins:
                CurrencyController.Coins += lootGot.amountOfLoot;
                _ttEventView.SpawnRewardsVFX(ParticleSpawner.ParticleObjects.Coins, lootGot.amountOfLoot);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
        _ttEventView._mainMenuView.UpdateMainMenuView();
    }

    public void HandleMaxAmountOfCombo(int amountOfCombo)
    {
        if (amountOfCombo >= 10)
        {
            TokensCollected += 3;
            return;
        }
        if(amountOfCombo >= 7)
        {
            TokensCollected += 2;
            return;
        }
        if (amountOfCombo >= 4)
        {
            TokensCollected += 1;
        }
    }
    private void FirstEventStartCheck()
    {
        if (currentEvent == TTEventDataModel.TTEventName.NeverStarted && LevelController.CurrentLevelIndex + 1 >= levelToUnlock) StartNextEvent();
    }
    
    // Check whether timer is out of time yet
    private void RealTimeCheck()
    {
        DateTime.TryParse(TtEventSavedStartTime, out TtStartTime);
        RealTimeTimer _realTimer = new RealTimeTimer(TtStartTime, timeForTimer);
        if (_realTimer.IsDone)
        {
            // Switch to next event
            StartNextEvent();
        }
        StartTimer(_realTimer.TimeLeft);
    }

    public void StartNextEvent()
    {
        var numberOfEvent = (int) currentEvent + 1;
        if (numberOfEvent > (int) TTEventDataModel.TTEventName.PostGodsEscapePause)
        {
            numberOfEvent = 1;
        }

        TokensCollected = 0;

        currentEvent = (TTEventDataModel.TTEventName) numberOfEvent;
        StartTimer(timeForTimer);
        TtStartTime = DateTime.Now;
        ShowDependingOnEvent();
        CurrentTier = 0;
        GameController.SaveGameData();
    }

    private void ShowDependingOnEvent()
    {
        switch (currentEvent)
        {
            case TTEventDataModel.TTEventName.ZooRama:
                _ttEventView.ShowMenuHolder();
                _ttEventView.UpdateProgress(TokensCollected, 
                    _ttEventDataModel.stepsToMake[CurrentTier]);
                break;
            case TTEventDataModel.TTEventName.SummerBreeze:
                _ttEventView.ShowMenuHolder();
                _ttEventView.UpdateProgress(TokensCollected, 
                    _ttEventDataModel.stepsToMake[CurrentTier]);
                break;
            case TTEventDataModel.TTEventName.GodsEscape:
                _ttEventView.ShowMenuHolder();
                _ttEventView.UpdateProgress(TokensCollected, 
                    _ttEventDataModel.stepsToMake[CurrentTier]);
                break;
            default:
                _ttEventView.HideMenuHolder();
                break;
        }
        UpdateTierAwards();
    }

    private void StartTimer(float time)
    {
        ttEventTimer = new Timer(time);
    }

    public void HideTheBar()
    {
        _ttEventView.HideMenuHolder();
    }

    public void ShowTheBar()
    {
        if (LevelController.CurrentLevelIndex + 1 < levelToUnlock) return;
        _ttEventView.ShowMenuHolder();
        _ttEventView.ChangeLocationToMenu();
    }

    public void ShowEndLevelBar()
    {
        if (LevelController.CurrentLevelIndex + 1 < levelToUnlock) return;
        _ttEventView.ShowMenuHolder();
        _ttEventView.ChangeLocationToEndScreen();
    }

    private void FixedUpdate()
    {
        TimeSpan timeSpan;
        if (currentEvent == TTEventDataModel.TTEventName.PostGodsEscapePause ||
            currentEvent == TTEventDataModel.TTEventName.NeverStarted ||
            currentEvent == TTEventDataModel.TTEventName.PostSummerBreezePause ||
            currentEvent == TTEventDataModel.TTEventName.PostZooRamaPause) return;
        timeSpan = TimeSpan.FromSeconds(ttEventTimer.TimeLeft);
        if (timeSpan.TotalMinutes <= 60)
        {
            _ttEventView.UpdateTimersText($"{timeSpan.ToString(@"mm\:ss")}");
            return;

        }
        _ttEventView.UpdateTimersText($"{(int)timeSpan.TotalHours}{timeSpan.ToString(@"\:mm\:ss")}");

    }
}



