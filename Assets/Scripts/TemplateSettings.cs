﻿using UnityEngine;
using Matchmania;

namespace HyperCasualTemplate
{
    public class TemplateSettings : MonoBehaviour
    {
        #region Singleton
        public static TemplateSettings instance;
        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance == this)
            {
                Destroy(gameObject);
            }
        }
        #endregion
    
        public bool firebase;
        public bool googleSignIn;
        public bool advertisement;

        public string webClientId = "<your client id here>";

        private void Start()
        {
            if (firebase)
            {
                gameObject.AddComponent<FirebaseInitializer>();
            }
            if(advertisement)
            {
                gameObject.AddComponent<AdvertisementSystem>();
            }
            DontDestroyOnLoad(gameObject);
        }
    }
}