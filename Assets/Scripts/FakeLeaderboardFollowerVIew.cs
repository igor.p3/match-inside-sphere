using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

namespace Matchmania
{
    public class FakeLeaderboardFollowerVIew : MonoBehaviour
    {
        public RectTransform transformToFollow;
        public Transform parent;

        private RectTransform rectTransform;

        public static float smoothTime = 0f;
        private Vector3 velocity = Vector3.zero;

        void Start()
        {
            rectTransform = GetComponent<RectTransform>();
            transform.SetParent(parent);
            //rectTransform.position = transformToFollow.position;
        }
        void Update()
        {
            if(transformToFollow == null)
            {
                Destroy(gameObject);
            }
            else
            {
                transform.position = Vector3.SmoothDamp(rectTransform.position, transformToFollow.position, ref velocity, smoothTime);
            }
        }
    }
}