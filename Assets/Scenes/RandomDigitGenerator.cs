using TMPro;
using UnityEngine;

public class RandomDigitGenerator : MonoBehaviour
{
    public TextMeshProUGUI text;
    public TextMeshProUGUI text_02;


    void Start()
    {
        int rnd = Random.Range(1, 9);
        text.text = $"{rnd}";
        text_02.text = $"{rnd}";
    }

   
}
