using System;
using TMPro;
using UnityEngine;

namespace Moonee
{
    public class GDPR : MonoBehaviour
    {
        public event Action OnCompleted;

        [SerializeField] private TextMeshProUGUI appName;

        // Use your saving system here if you want 
        public bool IsAlreadyAsked => PlayerPrefs.GetInt(SaveName, 0) > 0;

        private const string SaveName = "GDPR";

        private void Awake()
        {
            DontDestroyOnLoad(this);
        }

        private void Start()
        {
            var text = String.Format(appName.text, Application.productName);
            appName.text = text;
        }

        public void Ask()
        {
            if (IsAlreadyAsked) return;

            gameObject.SetActive(true);
        }

        // Called from unity event (on button click)
        public void OnAgreeButtonClicked()
        {
            OnCompleted?.Invoke();
            PlayerPrefs.SetInt(SaveName, 1);
            Close();
        }

        private void Close()
        {
            gameObject.SetActive(false);
        }
    }
}